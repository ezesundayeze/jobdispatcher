from django.shortcuts import render


def signin(request):

    context = {
        "title": "Log in"
    }
    return render(request, "employer/login.htm", context)


def create_task(request):
    """ """

    return render(request, "employer/create_task.html", {})
