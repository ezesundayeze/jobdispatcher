from . import views
from django.urls import path

urlpatterns = [
    path("login", views.signin, name="employerlogin"),
    path("create_task", views.create_task, name="create_task"),
]
