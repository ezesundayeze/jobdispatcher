from . import views
from django.urls import path

urlpatterns = [
    path("login", views.signin, name="employeelogin"),
    path("signup", views.signup, name="signup"),
    path("activate/<token>", views.activate, name="acitvate"),
    path("viewtask", views.view_task, name="view_task"),
    path('dashboard', views.dashboard, name="dashboard"),
    path('accept/<id>', views.accept_task, name="accept"),
    path('reject/<id>', views.reject_task, name='reject'),
    path('history', views.view_job_history, name='history'),
    path('profile', views.profile, name='profile'),
    path('profile_update/<id>', views.profile_update, name='profile_update'),
    path("skills_update/<id>", views.update_skills, name="skills_update"),
    path('skills', views.skills, name="skills"),
    path("notification", views.notification, name="notification"),
    path('read_notification/<id>', views.read_notification,
         name="read_notification"),
    path('logout', views.logout, name='logout'),
]
