from django.urls import reverse, resolve


class TestUrls:
    def test_details_url(self):
        path = reverse('history')
        assert resolve(path).view_name == "history"
