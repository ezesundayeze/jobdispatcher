from django.shortcuts import render, redirect, HttpResponse
from django.http import JsonResponse, request
from .models import Employees, Task, Skills, Notification
from django.dispatch import receiver
from django.db.models.signals import post_save
import bcrypt
from django.contrib.auth.hashers import make_password, check_password

# Email modules
from django.template.loader import get_template
import smtplib
import email.utils
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import uuid


def signin(request):
    """ This function allows a user to login and prevents unauthorized access """

    context = {
        "title": "Log in",
        "result": "",
    }
    # Making dure the request header contains a POST method.
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']

        # querying the database to get user data
        employee = Employees.objects.filter()
        for user in employee:

            # checking if password entered matches user's password.
            mpassword = check_password(password, user.password)
            # mypassword = bcrypt.hashpw(password.encode('utf-8'), user.password)
            if username == user.username and mpassword == True:
                if user.status == 1:
                    request.session['id'] = user.id
                    return redirect('dashboard')
                else:
                    context["result"] = "Please, check your email and verify your account in order to login"
            else:
                context["result"] = "Invalid login details"

    return render(request, "employee/login.html", context)


def signup(request):
    context = {
        "result": "",
    }

    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        name = request.POST['name']
        token = uuid.uuid4()
        try:
            employee = Employees.objects.get(username=username)
            context["result"] = "You are already registered."
        except:
            Employees(username=username, name=name, password=make_password(
                password, salt=None, hasher="bcrypt"), token=token).save()
            sendMail(request)
            context["result"] = "Registration was successful"

    return render(request, "employee/register.html", context)


def sendMail(request):
    # Replace sender@example.com with your "From" address.
    # This address must be verified.
    SENDER = 'support@qtochat.com'
    SENDERNAME = 'Job Dispatcher'

    # Replace recipient@example.com with a "To" address. If your account
    # is still in the sandbox, this address must be verified.
    RECIPIENT = request.POST['username']

    # Replace smtp_username with your Amazon SES SMTP user name.
    USERNAME_SMTP = "AKIAJ2O65UK4DN4KECBA"

    # Replace smtp_password with your Amazon SES SMTP password.
    PASSWORD_SMTP = "AlJFXrl/asM6hW0n2TDfyM8R8GUSIHRtHk7JivoVzgHN"

    # (Optional) the name of a configuration set to use for this message.
    # If you comment out this line, you also need to remove or comment out
    # the "X-SES-CONFIGURATION-SET:" header below.
    #CONFIGURATION_SET = "ConfigSet"

    # If you're using Amazon SES in an AWS Region other than US West (Oregon),
    # replace email-smtp.us-west-2.amazonaws.com with the Amazon SES SMTP
    # endpoint in the appropriate region.
    HOST = "email-smtp.us-east-1.amazonaws.com"
    PORT = 587

    # The subject line of the email.
    SUBJECT = 'Welcome to the Job Dispatcher'

    # The email body for recipients with non-HTML email clients.
    BODY_TEXT = (
        " Thank you for registering on Job Dispatcher"
    )

    # The HTML body of the email.
    userTitle = Employees.objects.get(username=request.POST['username'])
    context = {
        'title': userTitle.name,
        'token': userTitle.token,
    }
    htmlT = get_template(
        'employee/welcome_email.html').render(context)
    BODY_HTML = htmlT
    # Create message container - the correct MIME type is multipart/alternative.
    msg = MIMEMultipart('alternative')
    msg['Subject'] = SUBJECT
    msg['From'] = email.utils.formataddr((SENDERNAME, SENDER))
    msg['To'] = RECIPIENT

    # Comment or delete the next line if you are not using a configuration set
    # msg.add_header('X-SES-CONFIGURATION-SET',CONFIGURATION_SET)

    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(BODY_TEXT, 'plain')
    part2 = MIMEText(BODY_HTML, 'html')

    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)

    # Try to send the message.
    try:
        server = smtplib.SMTP(HOST, PORT)
        server.ehlo()
        server.starttls()
        # stmplib docs recommend calling ehlo() before & after starttls()
        server.ehlo()
        server.login(USERNAME_SMTP, PASSWORD_SMTP)
        server.sendmail(SENDER, RECIPIENT, msg.as_string())
        server.close()
    # Display an error message if something goes wrong.
    except Exception as e:
        print("Error: ", e)
    else:
        print("Email sent!")
    return render(request, "employee/register.html",
                  {"title": "Registered Successfully, please, check your email to activate your account"})


def activate(request, token):

    urlToken = Employees.objects.filter(token=token)
    if not urlToken:
        context = {
            'resultFailed':
            "Sorry, you don't have an account with us.\n Please, register. Thank you"
        }

        return render(request, 'employee/activate.html', context)
    if urlToken:
        for urlpart in urlToken:
            if urlpart.token == token and urlpart.status == 0:
                urlpart.status = "1"
                urlpart.save()

                context = {
                    "resultSuccess":
                    "Your account is fully activated. \n Please, log in"
                }
                return render(request, 'employee/activate.html', context)
            else:
                context = {
                    "result":
                    "Your account has been activated already, please log in"
                }
                return render(request, 'employee/activate.html', context)

    return render(request, 'employee/activate.html')


def view_task(request):
    context = {
        "Title": "View Task",
        "user": "",
        "task": "",
    }
    # print(request.session['id'])
    if 'id' in request.session:
        user = Employees.objects.get(id=request.session['id'])
        context['user'] = user
        task = Task.objects.filter(employee=request.session['id'])
        context['task'] = task
        # default settings
        context['user'] = user
        # show notifications
        notice = Notification.objects.filter(
            state="UNREAD", user_id=request.session['id'])

        # Show the number of unread notification available
        context['notice_count'] = notice.count()
        # Read the notifications available for all.
        context['notice_state'] = notice
        return render(request, "employee/viewtask.html", context)
    else:
        return redirect(signin)

    return render(request, "employee/viewtask.html", context)


def accept_task(request, id):
    """"Accept task"""
    task = Task.objects.get(id=id)
    if task.status == 0:
        task.status = 1
        task.employee = task.employee
        task.save()
    return redirect('view_task')


def reject_task(request, id):
    """" Reject Task"""
    task = Task.objects.get(id=id)
    if task.status == 0:
        task.status = None
        task.employee = task.employee
        task.save()
    return redirect('view_task')


def dashboard(request):
    context = {
        "Title": "View Task",
        "user": "",
        'notice_state': '',
    }
    # print(request.session['id'])
    if 'id' in request.session:
        user = Employees.objects.get(id=request.session['id'])
        context['user'] = user

        # show notifications
        notice = Notification.objects.filter(
            state="UNREAD", user_id=request.session['id'])

        # Show the number of unread notification available
        context['notice_count'] = notice.count()
        # Read the notifications available for all.
        context['notice_state'] = notice

        # for i in notice:
        #     print(i.time_stamp)

        return render(request, "employee/dashboard.html", context)
    else:
        return redirect(signin)

# if you click on the notification, it changes it's state to read.


def read_notification(request, id):
    notifi_query = Notification.objects.get(id=id)
    notifi_query.state = "READ"
    notifi_query.save()

    return redirect(view_task)


def profile(request):
    """ view personal profile """
    context = {
        "Title": "View Task",
        "user": "",
        "notice_state": "",
    }
    # print(request.session['id'])
    if 'id' in request.session:
        user = Employees.objects.get(id=request.session['id'])
        context['user'] = user
        print(request.session['id'])

        # default settings
        context['user'] = user

        # show notifications
        notice = Notification.objects.filter(
            state="UNREAD", user_id=request.session['id'])

        # Show the number of unread notification available
        context['notice_count'] = notice.count()
        # Read the notifications available for all.
        context['notice_state'] = notice

        return render(request, "employee/profile.html", context)
    else:
        return redirect(signin)


def profile_update(request, id):
    """ Update personal profile """
    context = {
        "Title": "View Task",
        "user1": "",
    }
    # print(request.session['id'])
    print(request.POST['username'])
    if 'id' in request.session:
        user = Employees.objects.get(id=request.session['id'])
        context['user1'] = user
        if request.method == "POST":
            user.name = request.POST['fullname']
            user.username = request.POST['username']
            # user.password = make_password(
            #     request.POST['password'], salt=None, hasher="bcrypt")
            user.save()
            return HttpResponse(
                "Update was successfull")
            # return HttpResponse(
            #     "Update was unsuccessfull")
        return HttpResponse("Not a post request")


def skills(request):
    """ update skill profile """
    context = {
        "user": "",
        "notice_count": "",
        "notice_state": "",
    }

    if "id" in request.session:
        user = Employees.objects.get(id=request.session['id'])
        # default settings
        context['user'] = user

        # show notifications
        notice = Notification.objects.filter(
            state="UNREAD", user_id=request.session['id'])

        # Show the number of unread notification available
        context['notice_count'] = notice.count()
        # Read the notifications available for all.
        context['notice_state'] = notice

        skills = Skills.objects.all()

        context["skills"] = skills
        return render(request, "employee/skills.html", context)
    else:
        return redirect(signin)


def update_skills(request, id):
    return HttpResponse("")


def view_job_history(request):
    context = {
        "Title": "View Task",
        "user": "",
        'task': '',
        "notice_count": "",
        "notice_state": "",
    }

    if "id" in request.session:
        task = Task.objects.filter(employee_id=request.session['id'])
        context['task'] = task
        user = Employees.objects.get(id=request.session['id'])
        # default settings
        context['user'] = user

        # show notifications
        notice = Notification.objects.filter(
            state="UNREAD", user_id=request.session['id'])

        # Show the number of unread notification available
        context['notice_count'] = notice.count()
        # Read the notifications available for all.
        context['notice_state'] = notice
        return render(request, 'employee/history.html', context)
    else:
        return redirect(signin)


def notification(data):
    print(data)
    return HttpResponse(data)
    # return JsonResponse(data, safe=False)


@receiver(post_save, sender=Notification)
def dispatchNofication(sender, instance, created, **kwargs):
    if created:
        my_data = {
            "title": instance.title,
            "message": instance.message,
        }
        return notification(my_data)


def logout(request):
    request.session.flush()
    return redirect(signin)
