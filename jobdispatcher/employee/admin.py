from django.contrib import admin
from .models import Employees, Skills, Task, Notification

admin.site.register(Employees)
admin.site.register(Task)
admin.site.register(Skills)
admin.site.register(Notification)

# Register your models here.
