# Generated by Django 2.1.3 on 2018-12-24 15:08

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('employee', '0003_auto_20181224_1508'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employees',
            name='token',
            field=models.CharField(default=uuid.UUID('b5f2aeaa-d257-4cf5-9012-9e231703fe38'), max_length=100),
        ),
    ]
