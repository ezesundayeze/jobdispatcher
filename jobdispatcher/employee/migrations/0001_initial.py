# Generated by Django 2.1.3 on 2018-12-24 15:06

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Employees',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField()),
                ('username', models.CharField(max_length=200, unique=True)),
                ('password', models.CharField(max_length=200)),
                ('token', models.CharField(default=uuid.UUID('b63cae37-bbfd-4781-8f65-e7de509e31f6'), max_length=100)),
                ('status', models.IntegerField(default=0)),
                ('task_accepted', models.IntegerField(default=0)),
                ('task_rejected', models.IntegerField(default=0)),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date created')),
            ],
        ),
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('message', models.TextField()),
                ('url', models.CharField(max_length=100)),
                ('state', models.CharField(choices=[('SEEN', 'seen'), ('READ', 'read'), ('UNREAD', 'unread')], default='UNREAD', max_length=20)),
                ('read_state', models.BooleanField(default=False)),
                ('time_stamp', models.DateTimeField(default=django.utils.timezone.now)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='employee.Employees')),
            ],
        ),
        migrations.CreateModel(
            name='Skills',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('jobdescription', models.TextField(max_length=300)),
                ('status', models.BooleanField(default=False, null=True)),
                ('created_date', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date created')),
                ('employee', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='employee.Employees')),
                ('skills', models.ManyToManyField(to='employee.Skills')),
            ],
        ),
        migrations.AddField(
            model_name='employees',
            name='skills',
            field=models.ManyToManyField(to='employee.Skills'),
        ),
    ]
