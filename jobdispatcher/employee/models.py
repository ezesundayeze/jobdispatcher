from django.db import models
from django.dispatch import receiver
import uuid
from django.http import JsonResponse, request
from django.utils import timezone
from django.db.models.signals import post_save


class Skills(models.Model):
    title = models.CharField(max_length=50)

    def __str__(self):
        return self.title


class Employees(models.Model):
    name = models.TextField()
    username = models.CharField(max_length=200, unique=True)
    password = models.CharField(max_length=200)
    token = models.CharField(default=uuid.uuid4(), max_length=100)
    status = models.IntegerField(default=0)
    skills = models.ManyToManyField(Skills)
    task_accepted = models.IntegerField(default=0)
    task_rejected = models.IntegerField(default=0)
    date_joined = models.DateTimeField('date created', default=timezone.now)

    def __str__(self):
        return self.username


class Notification(models.Model):
    """ Notification model """
    STATE_CHOICES = (
        ("SEEN", "seen"),
        ("READ", "read"),
        ("UNREAD", "unread")
    )
    title = models.CharField(max_length=200)
    message = models.TextField()
    user = models.ForeignKey(Employees, on_delete=models.CASCADE)
    url = models.CharField(max_length=100)
    state = models.CharField(
        max_length=20, choices=STATE_CHOICES, default="UNREAD")
    read_state = models.BooleanField(default=False)
    time_stamp = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.title


class Task(models.Model):
    title = models.CharField(max_length=200)
    jobdescription = models.TextField(max_length=300)
    skills = models.ManyToManyField(Skills)
    employee = models.ForeignKey(Employees, on_delete=models.CASCADE)
    status = models.BooleanField(default=False, null=True)
    created_date = models.DateTimeField('date created', default=timezone.now)

    def __str__(self):
        return self.title

    @property
    def taskExist(self):
        return self.title is not None


@receiver(post_save, sender=Task)
def notify(sender, instance, created, **kwargs):
    if created:
        Notification.objects.create(
            title="New Notification | {0}".format(instance.title),
            url="/",
            # state=Notification.state,
            time_stamp=timezone.now(),
            message=instance.jobdescription,
            user=instance.employee
        )
