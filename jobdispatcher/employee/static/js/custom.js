let form = document.getElementById("updateprofile");
let id = document.getElementById("userid").value;

form.addEventListener("submit", function(e) {
  e.preventDefault();
  $.ajax({
    type: "POST",
    url: `profile_update/${id}`,
    data: {
      fullname: $("#register-username").val(),
      username: $("#register-email").val(),
      password: $("#register-password").val(),
      csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val()
    },
    success: function(data) {
      alert(data);
      if (data === "Update was successfull") {
        $("#result").text("Update was successfull");
      } else {
        $("#result").text(
          "Update was not successfull, please, contact the administrator"
        );
      }
    },
    error: function(status, error, xhr) {
      alert(status + " " + error + " " + xhr);
    }
  });
});
